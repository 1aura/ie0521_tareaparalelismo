#include "../include/calculatePi.h"
#include "../include/multiplyVectors.h"
#include "../include/main.h"

/*
 * Global Variable Declaration
 */
bool DEBUG = true;
size_t NumThreads = 4;
int SamplesPerThread = 100000;
unsigned int ParallelHits = 0.0;
pthread_mutex_t PiMutex;
int* Vector = NULL;
int** Matrix = NULL;

/*
 * \fn 		main
 * \brief	Calls functions to calculate Pi and multiply vectors
 */
int main(){
	// Defines necessary parameters
	srand(time(NULL));
	int returnVal = 0;
	
	// Redefines global variable NumThreads
	NumThreads = (size_t)rand()%(9-3) + 3;
	SamplesPerThread = (int)(10000001/NumThreads);
	
	// In case of DEBUG, print the amount of threads 
	if(DEBUG){
		printf("\nPROBLEM 1: VECTOR-MATRIX MULTIPLICATION\n\n");
		printf("Vector: 1x%lu \tMatrix: %lux%lu ",NumThreads,NumThreads,NumThreads);
	}
	
	// Make vector V and matrix M
	vectorSetup();
	
	// Calls serial_Vectors
	int* serialResult = serial_Vector();
	
	// Calls parallel_Vectors
	int* parallelResult = parallel_Vector();
	
	// Compares them
	for(int j = 0; j< (int)NumThreads; j++){
		if (serialResult[j] != parallelResult[j]){
			returnVal = 1;
		} 
	}
	
	
	// In case of DEBUG, print the amount of threads and samples processed
	if(DEBUG){
		printf("\nPROBLEM 2: CALCULATING PI\n\n");
		printf("Number of Threads: %lu\nSamples per Thread: %d\n\n",NumThreads,SamplesPerThread);
	}
	
	// Calls serial_Pi
	double Pi_s = serial_Pi();
	
	// Calls parallel_Pi
	double Pi_p = parallel_Pi();
	
	// Compares them (to the first decimal place) and sets returnVal if there's a mistake
	if ((int)(Pi_s*10) != (int)(Pi_p*10)){
		returnVal = 1;
	}
	
	// If debugging, print both values:
	if(DEBUG){
		printf("\nResults:\nSerial: %f \tParallel: %f\n\n", Pi_s, Pi_p);
	}
	
	return returnVal;
}
