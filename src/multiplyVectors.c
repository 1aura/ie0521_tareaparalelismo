#include "../include/multiplyVectors.h"

extern bool DEBUG;
extern size_t NumThreads;
extern int* Vector;
extern int** Matrix;

void vectorSetup(){
	// Allocate level 1 space
	Vector = (int*)malloc(sizeof(int) * (int)NumThreads);
	Matrix = (int**)malloc(sizeof(int*) * (int)NumThreads);
	
	// Vector value update, allocate matrix level 2 space
	for (int i = 0; i < (int)NumThreads; i ++){
		Vector[i] = (int)rand()%(10-0);
		Matrix[i] = (int*)malloc(sizeof(int) * (int)NumThreads);
		// Print vector value during DEBUG
		if(DEBUG){
			printf("\nV[%d] %d\t",i,Vector[i]);
		}
		
		// Matrix value update
		for (int j = 0; j < (int)NumThreads; j ++){
			Matrix[i][j] = (int)rand()%(10-0);
			// Print matrix value during DEBUG
			if(DEBUG){
				printf(", M[%d][%d] %d ",i,j,Matrix[i][j]);
			}
		}
	}
	
	// Empty return
	return;
}

int* serial_Vector(){
	// Declare local variables
	int i,j;
	int row;
	int* result = (int*)malloc(sizeof(int) * (int)NumThreads);
		
	// Debug print
	if(DEBUG){
		printf("\n\nSerial Result: [ ");
	}
	for(i = 0; i < (int)NumThreads; i++){
		// multiply vector[i] times every element of matrix[i]
		row = 0;
		for(j = 0; j< (int)NumThreads; j++){
			row += Vector[i]*Matrix[i][j]; 
		}
		// set answer as result[i]
		result[i] = row;
		// Debug print
		if(DEBUG){
			printf("%d ",row);
		}
	}
	
	// Debug print
	if(DEBUG){
		printf("]\n");
	}
	
	// Divide hits by repetitions and return "pi"
	return result;
}


int* parallel_Vector(){
	// Declare local variables
	int j;
	size_t i;
	pthread_t handles[NumThreads];
	int* result = (int*)malloc(sizeof(int) * (int)NumThreads);
		
	// Debug print
	if(DEBUG){
		printf("Parallel Result: [ ");
	}
	
	// Update index
	for(j = 0; j< (int)NumThreads; j++){
		result[j] = j;
	}
	
	// Start the threads
	for (i = 0; i != NumThreads; ++i)
		pthread_create(&handles[i], NULL, thread_Mult, &result[i]);
	
	// Wait for the threads to complete
	for (i = 0; i != NumThreads; ++i)
		pthread_join(handles[i], NULL);
	
	// Debug print
	if(DEBUG){
		for(j = 0; j< (int)NumThreads; j++){
			printf("%d ", result[j]);
		}
		printf("]\n\n");
	}
	
	// Return result
	return result;
}


void* thread_Mult(void* threadParam){
	// Declare local variables
	int* ptr = (int*)(threadParam);
	int i = *ptr;
	int j;
	int row = 0;
	
	// Multiply vectors
	for(j = 0; j< (int)NumThreads; j++){
		row += Vector[i]*Matrix[i][j]; 
	}
	
	// Update result vector
	*ptr = row;
	
	// Return
	return threadParam;	
}


