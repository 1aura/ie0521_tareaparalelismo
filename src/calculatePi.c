#include "../include/calculatePi.h"

// Imports external variables
extern bool DEBUG;
extern size_t NumThreads;
extern int SamplesPerThread;
extern unsigned int ParallelHits;
extern pthread_mutex_t PiMutex;

double serial_Pi(){
	// Declare local variables
	int i = 0;
	unsigned int hits = 0;
	double x,y = 0.0;
	
	// MonteCarlo
	for (i=0; i<(int)(NumThreads*SamplesPerThread); i++){
		// Get two random numbers between 0 and 1
		x = (double)rand()/RAND_MAX;
		y = (double)rand()/RAND_MAX;
		// If sqrt(x*x + y*y) <=1, within the circle, increase hits
		if((x*x + y*y) <= 1.0){
			hits ++;
		}
	}
	
	// Debug print
	if(DEBUG){
		printf("Serial Hits: %d \tSerial Total: %d\n",(unsigned int)hits,(unsigned int)NumThreads*SamplesPerThread);
	}
	
	// Divide hits by repetitions and return "pi"
	return ((double)(4*(double)hits/((double)NumThreads*SamplesPerThread)));
}


double parallel_Pi(){
	// Reset global variables
	ParallelHits = 0;
	
	// Determine local variables
	size_t i;
	pthread_t handles[NumThreads];
	
	// Start the threads
	for (i = 0; i != NumThreads; ++i)
		pthread_create(&handles[i], NULL, thread_Pi, NULL);
	
	// Wait for the threads to complete
	for (i = 0; i != NumThreads; ++i)
		pthread_join(handles[i], NULL);
		
	// Divide hits by repetitions and return "pi" 
	return ((double)((double)ParallelHits/((double)NumThreads*SamplesPerThread))*4);
}


void* thread_Pi(void* threadParam){
	// Declare local variables
	int i;
	double x,y;
	unsigned int hits;
	
	// MonteCarlo
	for (i=0; i<(int)(SamplesPerThread); i++){
		// Get two random numbers between 0 and 1
		x = (double)rand()/RAND_MAX;
		y = (double)rand()/RAND_MAX;
		// If sqrt(x*x + y*y) <=1, within the circle, increase hits
		if((x*x + y*y) <= 1.0){
			hits ++;
		}
	}
	
	// Debug print
	if(DEBUG){
		printf("Thread Hits: %d \tThread Samples: %d\n",(unsigned int)hits,(unsigned int)SamplesPerThread);
	}
	
	// Mutex object
	pthread_mutex_lock(&PiMutex);
		ParallelHits += hits;
	pthread_mutex_unlock(&PiMutex);
	
	return threadParam;	
}
