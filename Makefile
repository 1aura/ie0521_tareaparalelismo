# Define the compiler and compilation flags
CC = g++
SPECS = -pthread
CFLAGS = -c -Wall

# Define the directories
IDIR = ./include
SDIR = ./src
ODIR = ./build

# List all header files and the final executable name
_HDRS = main.h calculatePi.h multiplyVectors.h
_OBJS = main.o calculatePi.o multiplyVectors.o
EXEC = tareaParalelismo

# Calculate the indirect header and object directions
HDRS = $(patsubst %,$(IDIR)/%,$(_HDRS))
OBJS = $(patsubst %,$(ODIR)/%,$(_OBJS))

# Compile all object files based on the corresponding c file
$(ODIR)/%.o:$(SDIR)/%.c $(HDRS)
	$(CC) $(SPECS) $(CFLAGS) -o $@ $<

# Obtain the executable based on the object files
$(EXEC):$(OBJS)
	$(CC) $(SPECS) $(OBJECTS) -o $@ $^

# Cleaning rule:
clean: 
	rm -f $(ODIR)/*.o

# Remove the executable
reset:
	rm -f $(EXEC)
