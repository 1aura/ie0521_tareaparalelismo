#ifndef CALCULATEPI
#define CALCULATEPI

#include <pthread.h>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>

double serial_Pi();

double parallel_Pi();

void *thread_Pi(void* threadParam);


#endif
