#ifndef MULTIPLYVECTORS
#define MULTIPLYVECTORS

#include <pthread.h>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>

void vectorSetup();

int* serial_Vector();

int* parallel_Vector();

void* thread_Mult(void* threadParam);

#endif
